// la cost è un indetinficatore alla quale dobbiamo dare un solo valore
const path = require('path');

// Serve ad esportare, per lo standard require / module.exports, dei valori / metodi 
module.exports = {
  // Il percorso al file iniziale della mia applicazione 
  //  ./significa => segui il percorso a partire dalla cartella in cui si trova questo file (webpack.config,js) 
  // src è il nome che viene universamente riconosciuto per la cartella che contiene i sorgenti dell'applicazione  
  entry: './src/index.js', // percorso relativo
  //mode imposta la modalità di lavoro di webpack affinchè vengano prodotti, qualora necessario, determinati tipi di files (ad es. i file di map)
  // development lo scriviamo perche lo stiamo lavorando in modalità di sviluppo
  mode: 'development',
  module: {
      rules: [{
        exclude: /node_modules/,
        use: [{
            loader: 'babel-loader',
        }],
      test: /\.jsx?$/,
    }]
  },

  // Definisce le informazioni relative alla destinzione del file che verrà generato da webpack
  output: {
    // _dirname è il nome della directory in cui mi trovo con tutto il percorso e viene messo nel "dist"
    // dist è il nome universamente riconosciuto per la cartella di distribuzione, che contiene il codice da eseguire nell'ambito di 
    //produzione
    //
    // Il contenuto di questa cartella NON deve mai essere modificata poichè viene generato automaticamente da webpack 
    // Questo è il percorso assoluto
    path: path.resolve(__dirname, 'dist'),
    // Definisce il nome del file all'interno del quale webpack inserirà il codice dell'applicazione
    //il file sarà posizionato all'interno della cartella specifica alla chiave "path"
    // bundle è il file finale prodotto in output
    filename: 'bundle.js',
  },
};