import { calcolo } from "./functions/Calculator";

var woman = document.getElementById("woman");
var colpa = document.getElementById("colpa");

woman.addEventListener("change", e => validator(e));

function validator (woman) {
    if(woman.target.value >= 0 && woman.target.value <= 100 ) {
        colpa.innerHTML = `La colpa è dello ${calcolo(woman.target.value)}%`;

        return true;
    } else {
        woman.value = '';
        colpa.innerHTML = '';

        return false;
    }
}